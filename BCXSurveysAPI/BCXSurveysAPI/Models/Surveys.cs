﻿using System;
using System.Collections.Generic;

namespace BCXSurveysAPI.Models
{
    public class Surveys
    {
        public Surveys()
        {
        }
        public string transactionId { get; set; }
        public List<Survey> surveys { get; set; } 
    }
}
