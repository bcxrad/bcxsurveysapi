﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using BCXSurveysAPI.Adapters;
using BCXSurveysAPI.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace BCXSurveysAPI.Controllers
{
    [Route("api/[controller]")]
    public class SurveysController : Controller
    {
        private string GET_SURVEYS_BY_LOCATION = "SELECT * FROM SURVEYS WHERE REGION = @region AND COMPANY = @company AND BUILDING = @building AND DEPARTMENT = @department";
        private string GET_SURVEY_RESPONSES_BY_EMP_ID = "SELECT * FROM SURVEY_RESPONSE WHERE EMPLOYEE_ID = @employeeId AND ID = @id";
        private string INSERT_SURVEY_RESPONSE = "INSERT INTO SURVEY_RESPONSES (ID, EMPLOYEE_ID, RESPONSE, RESPONSE_DATE) " +
            "VALUES (@id, @employeeId, @response, @date)";
        private string INSERT_NEW_SURVEY = "INSERT INTO SURVEYS (SURVEY_QUESTION, SURVEY_TYPE, REGION, BUILDING, COMPANY, DEPARTMENT, DATE) " +
            "VALUES(@question, @type, @region, @building, @company, @department, @date)";

        [HttpGet]
        public Surveys Get([FromBody]JObject employee)
        {
            string employeeId = (string)employee.GetValue("employeeId");
            string region = (string)employee.GetValue("region");
            string building = (string)employee.GetValue("building");
            string company = (string)employee.GetValue("company");
            string department = (string)employee.GetValue("department");
            dynamic surveys = new JObject();
            Surveys surveyList = new Surveys();

            Database db = new Database();
            try
            {
                db.Open();
                SqlCommand cmd = new SqlCommand(GET_SURVEYS_BY_LOCATION, db.GetConnection());
                SqlParameter regionParam = cmd.Parameters.Add("@region", SqlDbType.VarChar, 255);
                SqlParameter companyParam = cmd.Parameters.Add("@company", SqlDbType.VarChar, 255);
                SqlParameter buildingParam = cmd.Parameters.Add("@building", SqlDbType.VarChar, 255);
                SqlParameter departmentParam = cmd.Parameters.Add("@department", SqlDbType.VarChar, 255);
                regionParam.Value = region;
                companyParam.Value = company;
                buildingParam.Value = building;
                departmentParam.Value = department;
                SqlDataReader myReader = cmd.ExecuteReader();
                cmd.Parameters.Clear();
                while (myReader.Read())
                {
                    cmd = new SqlCommand(GET_SURVEYS_BY_LOCATION, db.GetConnection());
                    SqlParameter employeeIdParam = cmd.Parameters.Add("@employeeId", SqlDbType.VarChar, 255);
                    SqlParameter idParam = cmd.Parameters.Add("@id", SqlDbType.Int, 15);
                    employeeIdParam.Value = employeeId;
                    idParam.Value = int.Parse(myReader["id"].ToString());
                    SqlDataReader myReader2 = cmd.ExecuteReader();

                    if(myReader2.Read()) {
                        Survey survey = new Survey();
                        survey.question = myReader["survey_question"].ToString();
                        survey.id = myReader["id"].ToString();
                        surveyList.surveys.Add(survey);
                    }
                }
                surveys.surveys = JsonConvert.SerializeObject(surveyList);
            }
            catch (Exception e)
            {
                // news = e.ToString();
            }
            finally
            {
                db.Close();
            }
            return surveys;
        }

        [HttpPost]
        public void Post([FromBody]JObject survey)
        {
            Database db = new Database();
            try
            {
                db.Open();
                String source = (survey.GetValue("source") ?? String.Empty).ToString();
                if (source.Equals("admin"))
                {
                    SqlCommand sqlComm = (SqlCommand)db.RunQuery(INSERT_NEW_SURVEY);
                    sqlComm.Parameters.AddWithValue("@question", (string)survey.GetValue("question"));
                    sqlComm.Parameters.AddWithValue("@type", (string)survey.GetValue("type"));
                    sqlComm.Parameters.AddWithValue("@region", (string)survey.GetValue("region"));
                    sqlComm.Parameters.AddWithValue("@company", (string)survey.GetValue("company"));
                    sqlComm.Parameters.AddWithValue("@building", (string)survey.GetValue("building"));
                    sqlComm.Parameters.AddWithValue("@department", (string)survey.GetValue("department"));
                    sqlComm.Parameters.AddWithValue("@date", DateTime.Now);
                    sqlComm.ExecuteNonQuery();
                }
                else if (source.Equals("mobile")) 
                {
                    SqlCommand sqlComm = (SqlCommand)db.RunQuery(INSERT_SURVEY_RESPONSE);
                    sqlComm.Parameters.AddWithValue("@id", (string)survey.GetValue("id"));
                    sqlComm.Parameters.AddWithValue("@employeeId", (string)survey.GetValue("employeeId"));
                    sqlComm.Parameters.AddWithValue("@response", (string)survey.GetValue("response"));
                    sqlComm.Parameters.AddWithValue("@date", DateTime.Now);
                    sqlComm.ExecuteNonQuery();
                }

            }
            catch (Exception e)
            {
                //return e.ToString();
            }
            finally
            {
                db.Close();
            }
        }
    }
}
